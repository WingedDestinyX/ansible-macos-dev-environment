# macOS development machine setup

## Prerequisites

### __Development machine installed__

A clean install is just fine.

### __SSH enabled on the development machine

#### Via UI

1. Login on the development machine
2. Go to `System Preferences...`
3. Click on `Sharing`
4. Enable `Remote Login`

#### Via command line

```bash
sudo systemsetup -setremotelogin on
```

### __Homebrew locally installed__

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### __Ansible locally installed__

```bash
brew install ansible
```

#### __Ansible Configuration__

Make sure you are on the same network as the development machine is. Add the IP address or DNS of the development machine to the `./hosts` file.

Example:

```text
[machines]
xxx.xxx.xxx.xx1
xxx.xxx.xxx.xx2
xxx.xxx.xxx.xx3
```

After setting/creating this hosts file, you need to specify the `ANSIBLE_INVENTORY` env variable:

```bash
export ANSIBLE_INVENTORY=[full path]/hosts
```

&nbsp;

## Install development machine

If you run into problems in one of the following steps regarding sshpass, execute following command locally to solve this issue:

```bash
brew install http://git.io/sshpass.rb
```

### __Step 1__

- __Install Homebrew on the development machine ('`ssh [USERNAME_DEV_MACHINE]@[IP_ADDRESS_DEV_MACHINE]`'. Then__

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### __Step 2__

```bash
ansible-playbook -i ./hosts install.yml --ask-pass --ask-become-pass
```

If something is asked simular to the lines below, type `yes` and press `enter`.

```bash
paramiko: The authenticity of host '[IP_ADDRESS_DEV_MACHINE]' can't be established.
The ssh-ed25519 key fingerprint is [OWN_FINGERPRINT].
Are you sure you want to continue connecting (yes/no)?
```

&nbsp;

## Possible Ansible playbooks

Run an Ansible playbook (replace the `[NAME]` part):

```bash
ansible-playbook -i ./hosts [NAME].yml --ask-pass --ask-become-pass
```

| Name                   | Description |
| ---------------------- | ------------- |
| install                | Install Appstore updates, Java, Gems and configures the machine |
| install-tools          | Install iTerm2 + theme and lots of applications |
| reboot                 | Reboot the physical machine |
